## freeShop

This is the EOL version of freeShop. If you want to get the original freeShop source code, roll back to commit `7b9e5b2070`. If you want to use freeShop for some reason (note: the application doesn't work, see below for why), compile it with the instructions in the wiki and stub out the update checker.

It's pretty much a stubbed application showing two screens.

Use latest citro2d if you want to compile this for some reason.

## Why freeShop broke

On July the 30th, 2018, Nintendo released 3DS system update 11.8.0-41. This system update changed the `nim` module (which is used by the 3DS itself to install titles and system updates) to send along two new headers. One of these headers is a base64 version of the ticket.

Tickets on the 3DS are signed by a key that Nintendo only has and is not considered Reverse Engineerable (meaning it's not possible to make your own correctly signed tickets). When CFW is installed (such as Luma3DS), it is possible to disable the signature checks for these tickets on the system itself. 

Programs like freeShop make their own tickets, which are considered incorrectly signed.

On 9th of August, 2018, Nintendo started enabling requiring these headers on a small number of titles, mostly 1st and 2nd party titles. Before 9th of August, while tickets would not _need_ to be send along, Nintendo would still verify and reject downloads with an incorrectly signed ticket (this could be seen in freeShop by trying to do a sleep download, which uses the `nim` module, which would subsequently fail).

On 22nd of August, Nintendo expanded this change to all of their commercial applications.

Source: [3dbrew](https://www.3dbrew.org/wiki/11.8.0-41)

# ( ͡° ͜ʖ ͡°)
